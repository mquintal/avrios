# avrios-project

## how to install

```shell
npm install
```

## how to run
```shell
npm run start
```

## how to test
```shell
npm run test
```
