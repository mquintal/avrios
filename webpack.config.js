const ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

module.exports = {
  context: __dirname + '/app',
  entry: './index.js',
  output: {
    path: __dirname + '/app',
    filename: 'bundle.js',
  },
  module: {
    rules: [
      { test: /\.js$/,
        use: [
          // { loader: 'ng-annotate-loader' },
          { loader: 'babel-loader' },
        ],
        exclude: /node_modules/,
      },
      { test: /\.html$/, loader: 'raw-loader', exclude: /node_modules/ },
      { test: /\.scss$/, loaders: ['style-loader', 'css-loader', 'sass-loader'], exclude: /node_modules/ },
    ]
  },
  plugins: [
    new ngAnnotatePlugin({ add: true }),
  ],
  devtool: 'source-map',
};
