export default {
  API: {
    PROTOCOL: 'https',
    DOMAIN: 'api.backendless.com',
    PATH: '23F61471-A900-2FAC-FFEB-70FD129A1E00/5F5A3193-519C-1FA3-FFD9-9385C428BD00/data',
    PAGE_SIZE: 10,
  },
  COUNTER: 45,
};
