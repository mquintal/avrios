export default function($routeProvider) {
  'ngInject';

  $routeProvider
  .when('/user',{
     template: require('./user.view.html'),
     controller:'userController',
     controllerAs:'vm',
    });
}
