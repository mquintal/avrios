export default class UserController {
    constructor(User, $location) {
        'ngInject';
        this.User = User;
        this.$location = $location;
    }

    add(name) {
      this.User.add(name);
      this.$location.path('/');
    }
}
