import angular from 'angular'
import userController from './user.controller';
import userRoute from './user.route';

export default angular.module('user.page', [])
    .controller('userController', userController)
    .config(userRoute);
