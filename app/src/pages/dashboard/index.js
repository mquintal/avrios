import angular from 'angular';
import 'angular-route';
import coontroller from './dashboard.controller';
import routes from './dashboard.route';
import wordsBuffer from './wordsbuffer.service';

import './dashboard.style.scss';

export default angular.module('dashboard.page', ['ngRoute'])
  .controller('DashboardController', coontroller)
  .service('WordsBuffer', wordsBuffer)
  .config(routes);
