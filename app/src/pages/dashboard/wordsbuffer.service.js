export default class WordsBuffer {
  constructor(Utils, Words, CONFIG) {
    'ngInject';
    this.Utils = Utils;
    this.Words = Words;
    this.CONFIG = CONFIG;
    this.page = 1;
    this.data = {
      words: [],
      index: -1,
      count: 0,
      current: '',
    };
  }

  /**
   * Loads the page of words.
   *
   * @return {void}
   */
  loadNextPage() {
      if( this.data.index + Math.floor(this.CONFIG.API.PAGE_SIZE/2) === (this.page * this.CONFIG.API.PAGE_SIZE) ) {
          this.Words.get(this.page+1).then((words) => {
              this.data.words = this.data.words.concat(words);
              this.data.count = words.length;
              this.page++;
          });
      }
  }

  /**
   * Sets the word.
   *
   * @return {void}
   */
  next() {
    this.data.index++;
    let word = this.data.words[this.data.index];
    let shuffled = this.Utils.shuffle(word.split(''));
    this.data.current = shuffled.join('');
    this.loadNextPage();
  }

  /**
   * Start the process loading the first page of words.
   *
   * @return {Promise}
   */
  start() {
      this.page = 1;
      return this.Words.get(this.page).then((words) => {
          this.data.words = words;
          this.data.index = -1;
          this.data.count = words.length;
          this.next();
          return this.data;
      });
  }
}
