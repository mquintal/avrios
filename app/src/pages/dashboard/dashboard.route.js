export default function($routeProvider) {
  'ngInject';

  $routeProvider
   .when('/',{
     template: require('./dashboard.view.html'),
     controller:'DashboardController',
     controllerAs:'vm',
     resolveRedirectTo: function(User) {
         'ngInject';

         if( !User.exist() ) {
             return '/user';
         }
     },
   })
}
