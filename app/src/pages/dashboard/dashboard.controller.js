export default class DashboardController {
  constructor(Words, Counter, Utils, WordsBuffer, Score) {
    'ngInject';
    this.Words = Words;
    this.Counter = Counter;
    this.utils = Utils;
    this.WordsBuffer = WordsBuffer;
    this.Score = Score;
    this.data = {
      counter: Counter.data,
      buffer: this.WordsBuffer.data,
      score: this.Score.data,
      loading: false,
    };
  }

  onChangeWord(e) {
    const word = this.data.buffer.words[this.data.buffer.index];

    if( this.Score.check(e.target.value, word) ) {
      this.WordsBuffer.next();
      e.target.value = '';
    };
  }

  start() {
      this.data.loading = true;
      this.WordsBuffer.start().then(() => {
          this.Counter.start();
          this.data.loading = false;
      });
  }
}
