export default class MainController {
    constructor(User, Score, $scope) {
        'ngInject';
        this.data = {
            username: User.get(),
            score: Score.data
        }

        $scope.$watch(() => User.get(), (newV) => { this.data.username = newV });
    }
}
