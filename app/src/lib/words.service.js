export default class Words {
  constructor(Fetch, CONFIG) {
    'ngInject';
    this.Fetch = Fetch;
    this.CONFIG = CONFIG;
  }

  /**
   * Responsible for getting a list/array of words.
   *
   * @param {Number} page
   * @return {Promise}
   */
  get(page = 1) {
    return this.Fetch
    .get('words', {
      pageSize: this.CONFIG.API.PAGE_SIZE,
      offset: (page - 1) * this.CONFIG.API.PAGE_SIZE,
      props: 'content'
    })
    .then(({data}) => {
      return (data || []).map(item => item.content);
    });
  }
}
