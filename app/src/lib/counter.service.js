export default class Counter {
  constructor($interval, CONFIG) {
    'ngInject';
    this.$interval = $interval;
    this.CONFIG = CONFIG;
    this.data = {
      seconds: CONFIG.COUNTER,
      running: false,
    };
  }

  /**
   * Inits counter.
   *
   * @return {Promise}
   */
  start() {
    this.data.running = true;
    this.data.seconds = this.CONFIG.COUNTER;

    return this.$interval(() => {
      this.data.seconds--;
    }, 1000, this.CONFIG.COUNTER)
    .then(() => {
      this.data.running = false;
    });
  }
}
