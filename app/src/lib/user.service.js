export default class User {
  constructor(Storage) {
    'ngInject';
    this.Storage = Storage;
  }

  /**
   * Check if there is a user already.
   *
   * @return {Boolean}
   */
  exist() {
    const db = this.Storage.get();
    return !!db.user;
  }

  /**
   * Add a use to the storage.
   *
   * @param {String} name
   * @return {Void}
   */
  add(name) {
    const db = this.Storage.get();

    this.Storage.set(Object.assign({}, db, {user: name}));
  }

  /**
   * Get storaged user name.
   *
   * @return {String}
   */
  get() {
    return this.exist() ? this.Storage.get().user : '';
  }
}
