export default class Fetch {
  constructor($http, CONFIG) {
    'ngInject';

    this.$http = $http;
    this.CONFIG = CONFIG;
  }

  /**
   * Get all data from
   *
   * @param {String} path
   * @param {String} query
   * @return {Promise}
   */
  get(path, query) {
    const url = `${this.CONFIG.API.PROTOCOL}://${this.CONFIG.API.DOMAIN}/${this.CONFIG.API.PATH}`;

    return this.$http.get(`${url}/${path}`, { params: query });
  }
}
