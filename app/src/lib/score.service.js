export default class Score {
  constructor() {
    'ngInject';

    this.data = {
      score: 0,
      previous: '',
    };
  }

  /**
   * Decrements current score.
   *
   * @return {void}
   */
  decrementScore() {
    if( this.data.score > 0 ) {
      this.data.score--;
    }
  }

  /**
   * Adds value to the score based on the word provided.
   *
   * @param {String} word
   * @return {void}
   */
  addWordScore(word) {
    const n = word.length;
    this.data.score += Math.floor( Math.pow( 1.95, n/3 ) );
  }

  /**
   * Checks if the value provided matchs with the word provided.
   *
   * @param {String} value
   * @param {String} word
   * @return {Boolean}
   */
  check(value, word) {
    if (this.data.previous.length > value.length ) {
      this.decrementScore();
    }
    else if( value === word ) {
      this.addWordScore(word);
      this.data.previous = '';
      return true;
    }
    this.data.previous = value;
    return false
  }
}
