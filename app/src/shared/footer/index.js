import angular from 'angular';
import Component from './footer.component';

export default angular.module('shared.footer', [])
    .component('componentFooter', Component);
