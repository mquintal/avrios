const component = {
    bindings: {
        username: '<',
        score: '<',
    },
    transclude: true,
    template: require('./header.view.html'),
    controllerAs: 'vm',
};

export default component;
