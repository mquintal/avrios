import angular from 'angular';
import Component from './header.component';

export default angular.module('shared.header', [])
    .component('componentHeader', Component);
