import angular from 'angular';
import spinner from './spinner.component';

import './spinner.style.scss';

export default angular.module('shared.spinner', [])
    .component('spinner', spinner);
