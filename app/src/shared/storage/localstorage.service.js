export default class LocalStore {
  constructor($window) {
    'ngInject';
    this.window = $window;
  }

  get () {
    return JSON.parse(this.window.localStorage.getItem('db') || '{}')
  }

  set (data) {
      this.window.localStorage.setItem('db', JSON.stringify(data));
  }
}
