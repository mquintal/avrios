export default class Storage {
  constructor(LocalStorage) {
    'ngInject';
    this.LocalStorage = LocalStorage;
  }

  get() {
    return this.LocalStorage.get();
  }

  set(data) {
    return this.LocalStorage.set(data);
  }
}
