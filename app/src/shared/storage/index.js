import angular from 'angular';
import LocalStore from './localstorage.service';
import Storage from './storage.service';

export default angular.module('shared.storage', [])
  .service('LocalStorage', LocalStore)
  .service('Storage', Storage);
