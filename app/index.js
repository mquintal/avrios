import angular from 'angular';

// Shared Components
import storage from './src/shared/storage';
import footer from './src/shared/footer';
import header from './src/shared/header';
import spinner from './src/shared/spinner';

// Pages
import dashboard from './src/pages/dashboard';
import userPage from './src/pages/user';

// Main Controller
import MainController from './src/main.controller';

// Common services/confis
import CONFIG from './src/config';
import fetch from './src/lib/fetch.service';
import words from './src/lib/words.service';
import user  from './src/lib/user.service';
import counter from './src/lib/counter.service';
import utils from  './src/lib/utils.service';
import score from './src/lib/score.service';

import './scss/main.scss';

const app = angular.module('app', [
    storage.name,
    dashboard.name,
    footer.name,
    userPage.name,
    header.name,
    spinner.name]
)
.controller('MainController', MainController)
.constant('CONFIG', CONFIG)
.service('Fetch', fetch)
.service('Words', words)
.service('User', user)
.service('Counter', counter)
.service('Utils', utils)
.service('Score', score);
