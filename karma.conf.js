

module.exports = function karmaConfig (config) {
  config.set({
    frameworks: [
      // Reference: https://github.com/karma-runner/karma-jasmine
      // Set framework to jasmine
      'jasmine'
    ],

    reporters: [
      // Reference: https://github.com/mlex/karma-spec-reporter
      // Set reporter to print detailed results to console
      'progress',

      // Reference: https://github.com/karma-runner/karma-coverage
      // Output code coverage files
      'coverage'
    ],

    files: [
      // Grab all files in the app folder that contain .spec.
      'tests/tests.webpack.js',
    ],

    preprocessors: {
      // Convert files with webpack and load sourcemaps
      'tests/tests.webpack.js': ['webpack', 'sourcemap'],
    },

    browsers: [
      // Run tests using PhantomJS
      'Chrome'
    ],

    autoWatch: true,

    usePolling: true,

    singleRun: true,

    webpack: require('./webpack.config'),

    // Hide webpack build information from output
    webpackMiddleware: {
      noInfo: 'errors-only'
    }
  });
};
