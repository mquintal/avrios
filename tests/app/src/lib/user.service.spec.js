import '../../../../app';

let victim, Storage;

describe( 'app/lib/user.service.js', () => {
    beforeEach(angular.mock.module('app'));

    beforeEach(inject((_$injector_) => {
        victim = _$injector_.get('User');
        Storage = _$injector_.get('Storage');
    }));

    beforeEach(() => {
        spyOn(Storage, 'get');
        spyOn(Storage, 'set');
    });

    describe('when is exist method is called', () => {
        beforeEach(() => {
            Storage.get.and.returnValue({});
        });

        it('should called Storage.get method', () => {
            victim.exist()
            expect(Storage.get).toHaveBeenCalled();
        });

        it('should return false when there is no user', () => {
            expect(victim.exist()).toBe(false);
        });

        it('should return false when there is a user', () => {
            Storage.get.and.returnValue({user: 'imauser'});
            expect(victim.exist()).toBe(true);
        });
    });

    describe('when is add method is called', () => {
        const name = 'imauser';

        beforeEach(() => {
            Storage.get.and.returnValue({});
            victim.add(name);
        });

        it('should call Storage.get method', () => {
            expect(Storage.get).toHaveBeenCalled();
        });

        it('should call Storage.set with the expect object', () => {
            expect(Storage.set).toHaveBeenCalledWith({user: name});
        });
    });

    describe('when is get method is called', () => {
        it('should provide empty string when user does not exist', () => {
            Storage.get.and.returnValue({});
            expect(victim.get()).toEqual('');
        });

        it('should provide the stored user', () => {
            Storage.get.and.returnValue({user:'imauser'});
            expect(victim.get()).toEqual('imauser');
        });
    });
});
