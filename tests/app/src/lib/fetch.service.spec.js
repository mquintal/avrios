import '../../../../app';

let victim, CONFIG, $http, $q;

describe( 'app/lib/fetch.service.js', () => {
    beforeEach(angular.mock.module('app'));

    beforeEach(inject((_$injector_) => {
        victim = _$injector_.get('Fetch');
        $http = _$injector_.get('$http');
        CONFIG = _$injector_.get('CONFIG');
        $q = _$injector_.get('$q');
    }));

    beforeEach(() => {
        spyOn($http, 'get').and.returnValue($q.resolve([]));
    });

    describe('when is get method is called', () => {
        it('should called $http.get with the expected parameters', () => {
            const path = 'some/path';
            const query = { some: 'query data' };

            victim.get(path, query);
            expect($http.get).toHaveBeenCalledWith(
                `${CONFIG.API.PROTOCOL}://${CONFIG.API.DOMAIN}/${CONFIG.API.PATH}/${path}`,
                { params: query }
            );

        });
    });
});
