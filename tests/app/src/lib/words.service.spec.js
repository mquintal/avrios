import '../../../../app';

let victim, Fetch, CONFIG, $q;

describe( 'app/lib/words.service.js', () => {
    beforeEach(angular.mock.module('app'));

    beforeEach(inject((_$injector_) => {
        victim = _$injector_.get('Words');
        Fetch = _$injector_.get('Fetch');
        CONFIG = _$injector_.get('CONFIG');
        $q = _$injector_.get('$q');
    }));

    beforeEach(() => {
        spyOn(Fetch, 'get').and.returnValue($q.resolve([]));
    });

    describe('when is get method is called', () => {
        it('should called Fetch.get with the expected parameters', () => {
            const page = 2;

            victim.get(page);
            expect(Fetch.get).toHaveBeenCalledWith('words', {
                pageSize: CONFIG.API.PAGE_SIZE,
                offset: (page - 1) * CONFIG.API.PAGE_SIZE,
                props: 'content',
            });
        });
    });
});
