import '../../../../../app/src/shared/storage';

let victim, LocalStorage;

describe('app/shared/storage/localstorage.service.js', () => {
  beforeEach(angular.mock.module('shared.storage'));

  beforeEach(angular.mock.inject((_$injector_) => {
    LocalStorage = _$injector_.get('LocalStorage');
    victim = _$injector_.get('Storage');
  }));

  beforeEach(() => {
    spyOn(LocalStorage, 'set');
    spyOn(LocalStorage, 'get');
  });

  describe('when get method is called', () => {
     it('should called LocalStorage.get method', () => {
        victim.get();
        expect(LocalStorage.get).toHaveBeenCalled();
     });
  });

  describe('when set method is called', () => {
     it('should called LocalStorage.get method', () => {
        const data = { some: 'data' };
        victim.set(data);
        expect(LocalStorage.set).toHaveBeenCalledWith(data);
     });
  });

});
