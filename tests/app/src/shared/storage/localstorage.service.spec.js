import '../../../../../app/src/shared/storage';

let victim, $window;

describe('app/shared/storage/localstorage.service.js', () => {
  beforeEach(angular.mock.module('shared.storage'));

  beforeEach(angular.mock.inject((_$injector_) => {
    victim = _$injector_.get('LocalStorage');
    $window = _$injector_.get('$window');
  }));

  beforeEach(() => {
      spyOn($window.localStorage, 'getItem');
      spyOn($window.localStorage, 'setItem');

  });

  describe('when get method is executed', () => {
      it('should return a empty object when there is no storage data', () => {
         $window.localStorage.getItem.and.returnValue(null);
         expect(victim.get()).toEqual({});
      });

      it('should return the expected object when data exists', () => {
         const data = '{ "some": "data" }';

         $window.localStorage.getItem.and.returnValue(data);
         expect(victim.get()).toEqual(JSON.parse(data));
      });
  });

  describe('when set method is executed', () => {
      it('should call localStorage.setItem with the expect value', () => {
         const data = { some: 'data' };

         victim.set(data);
         expect($window.localStorage.setItem).toHaveBeenCalledWith(jasmine.any(String), JSON.stringify(data));
      });
  });
});
